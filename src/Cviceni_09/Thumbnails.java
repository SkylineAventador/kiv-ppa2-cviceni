package Cviceni_09;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Thumbnails {
    private static String[] fileNames;
    private static int[] C = new int[]{1000, 1009, 30030, 100000, 100003};
    private static long start, end;

    public static void main(String[] args) {
        readFromFile("./cv09_data/cv09_data.txt");

        HashTable table;
        Random random = new Random();
        int randomIndex = 0;
        int queriesCount = 0;

        //Hloupa rozptylovaci funkce
        System.out.println("Počet zpracovaných dotazů za 2 sekundy, hloupá rozptylová funkce:");
        for (int value : C) {
            table = new HashTable(value);
            randomIndex = random.nextInt(fileNames.length);

            start = System.currentTimeMillis();
            do {
                if (table.get(fileNames[randomIndex]) == -1) {
                    table.add(fileNames[randomIndex], 0);
                }
                end = System.currentTimeMillis();
                queriesCount++;
            } while (end - start < 2000);
            System.out.printf("Pro C = %d bylo zpracováno %d dotazů\n", value, queriesCount);
        }
        queriesCount = 0;

        //Chytra rozptylovaci funkce
        System.out.println("Počet zpracovaných dotazů za 2 sekundy, chytrá rozptylová funkce:");
        for (int value : C) {
            table = new HashTable(value);
            randomIndex = random.nextInt(fileNames.length);

            start = System.currentTimeMillis();
            do {
                if (table.get(fileNames[randomIndex]) == -1) {
                    table.add(fileNames[randomIndex], table.getHashCode(fileNames[randomIndex]));
                }
                end = System.currentTimeMillis();
                queriesCount++;
            } while (end - start < 2000);
            System.out.printf("Pro C = %d bylo zpracováno %d dotazů\n", value, queriesCount);
        }
    }

    private static int countValuesAtIndex(HashTable table, int tableIndex) {
        int counter = 0;
        Entry last = table.data[tableIndex];
        while (last != null) {
            counter++;
            last = last.next;
        }
        return counter;
    }

    private static String RandomImageName() {
        Random r = new Random();
        int year = 2005 + r.nextInt(13);
        int month = 1+r.nextInt(12);
        int day = 1+r.nextInt(28);
        int img = 1+r.nextInt(9999);
        return String.format("c:\\fotky\\%d-%02d-%02d\\IMG_%04d.CR2", year, month, day, img);
    }

    private static void fillTable(HashTable table, int valuesCount) {
        for (int i = 0; i < valuesCount; i++) {
            String value = RandomImageName();
            table.add(value, table.getHashCode(value));
        }
        System.out.println("Table was successfully filled with random values.");
    }

    private static void readFromFile(String filePath) {
        int counter = 0;
        try {
            Scanner scanner = new Scanner(new File(filePath));
            while (scanner.hasNextLine()) {
                counter++;
                scanner.nextLine();
            }
            scanner.close();
            fileNames = new String[counter];
            counter = 0;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath));
            String readLine = bufferedReader.readLine();
            while (readLine != null) {
                fileNames[counter] = readLine;
                counter++;
                readLine = bufferedReader.readLine();
            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Data were successfully read from the file");
    }
}
