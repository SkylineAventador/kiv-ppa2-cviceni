package Cviceni_09;

public class Entry {
    public String fileName;
    int indexInDatabase;
    public Entry next;

    public Entry (String fn, int index) {
        fileName = fn;
        indexInDatabase = index;
    }
}
