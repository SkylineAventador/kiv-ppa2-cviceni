package Cviceni_09;

public class HashTable {
    Entry[] data;

    public HashTable(int capacity) {
        data = new Entry[capacity];
    }

    public void add(String key, int value) {
        Entry newEntry = new Entry(key, value);
        int index = getHashCode(key);
        newEntry.next = data[index];
        data[index] = newEntry;
    }

    int getHashCode(String s){
        int r = s.charAt(0);
        for (int i = 1; i < s.length(); i++) {
            r = (r * 255 + s.charAt(i)) % data.length;
        }
        return r;
    }

    public int get(String s){
        int hash = getHashCode(s);
        Entry current = data[hash];
        while (current != null) {
            if (current.fileName.equals(s))
                return (current.indexInDatabase);
            current = current.next;
        }
        return -1;
    }

}
