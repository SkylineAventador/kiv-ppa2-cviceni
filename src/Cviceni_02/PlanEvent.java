package Cviceni_02;

enum Subject {math, computers}


public class PlanEvent {
    public String tutor; // name
    public int start; // hour
    public int end; // hour
    public int dayOfWeek; // 0=Monday, 1=Tuesday etc.
    public Subject subject;

    public PlanEvent(String tutor, Subject subject, int dayOfWeek, int start, int end) {
        this.tutor = tutor;
        this.start = start;
        this.end = end;
        this.dayOfWeek = dayOfWeek;
        this.subject = subject;
    }

    public boolean isInConflict(PlanEvent other) {
            if (this.dayOfWeek == other.dayOfWeek) {
                return this.start < other.end && other.start < this.end;
            }
        return false;
    }

    public void printTutorInfo() {
        System.out.print("Jmeno: " + this.tutor + "\t||\t");
        System.out.print("Predmet: " + this.subject + "\t||\t");
        System.out.print("Den: " + this.dayOfWeek + "\t||\t");
        System.out.print("Zacatek: " + this.start + "\t||\t");
        System.out.println("Konec: " + this.end);
    }

    public static void main(String[] args) {
        Plan plan = new Plan(".\\cv2_data\\ssc.txt");
        plan.generateQuintets();
    }
}
