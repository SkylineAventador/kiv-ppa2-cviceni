package Cviceni_02;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;


public class Plan {
    private final int EVENT_PARAM_COUNT = 5; // Pocet parametru kazde nabidky ze souboru.
    private final int MAX_PRINT_EVENTS_COUNT = 30; //Hranice poctu nabidek pro vypis do konzole po nacteni ze souboru.
    private PlanEvent[] events;
    private ArrayList<Plan> allQuintets;
    public String isOK_answer = "";

    public Plan(PlanEvent[] events) {
        this.events = events;
    }

    public Plan(String filePath) {
        try {
            Scanner scanner = new Scanner(new File(filePath));
            System.out.println("Probiha nacitani souboru... Cekejte prosim.");
            int fileSize = 0;
            while (scanner.hasNextLine()) {
                fileSize++;
                scanner.nextLine();
            }
            System.out.println("Nacitani bylo dokonceno. Nalezeno " + (fileSize / EVENT_PARAM_COUNT) + " zaznamu.\n" +
                    "Pripravuji data ke zpracovani...Cekejte prosim.");
            fileSize /= EVENT_PARAM_COUNT;
            events = new PlanEvent[fileSize];
            scanner = new Scanner(new File(filePath));
            for (int i = 0; i < fileSize; i++) {
                events[i] = new PlanEvent(
                        scanner.nextLine(),
                        scanner.nextLine().equals("math") ? Subject.math : Subject.computers,
                        Integer.parseInt(scanner.nextLine()),
                        Integer.parseInt(scanner.nextLine()),
                        Integer.parseInt(scanner.nextLine())
                );
            }
            if (events.length <= MAX_PRINT_EVENTS_COUNT) {
                System.out.println("OK: Data jsou zpracovana. Vypisuju info o tutorech:\n");
                Arrays.stream(events).forEach(PlanEvent::printTutorInfo);
            } else {
                System.out.println("OK: Data jsou zpracovana.");
            }

        } catch (FileNotFoundException e) {
            if (filePath.contains("\\")) {
                System.out.println("Chyba: Systém nemůže nalézt soubor \"" +
                        filePath.substring(filePath.lastIndexOf('\\') + 1) + "\"");
            } else
                System.out.println("Chyba: Systém nemůže nalézt soubor \"" + filePath + "\"");
        }
    }

    public boolean isConflict() {
        for (PlanEvent event: events) {
            for (PlanEvent event1: events) {
                    if(event1.isInConflict(event) && !event1.equals(event))
                        return true;
            }
        }
        return false;
    }

    public boolean isOK() {
        ArrayList<Integer> mathDays = new ArrayList<>(events.length);
        ArrayList<Integer> computersDays = new ArrayList<>(events.length);

        for (PlanEvent event: events) {
            if (event.subject.equals(Subject.math)) {
                mathDays.add(event.dayOfWeek);
            } else {
                computersDays.add(event.dayOfWeek);
            }
        }

        if (mathDays.size() >= 3 && computersDays.size() >= 2) {
            Collections.sort(mathDays);
            Collections.sort(computersDays);
            int lastValue = 0;

            for (Integer day : computersDays) {
                if (day == lastValue) {
                    isOK_answer = "Kolize dnu";
                    return false;
                }
                lastValue = day;
            }

            lastValue = 0;
            for (Integer day : computersDays) {
                if (day == lastValue) {
                    isOK_answer = "Kolize dnu";
                    return false;
                }
                lastValue = day;
            }
        } else {
            isOK_answer = "Nedostatecny pocet hodin";
            return false;
        }
        return true;
    }

    private void printTutorsInfo() {
        for (PlanEvent event: events) {
            event.printTutorInfo();
        }
        System.out.println('\n');
    }

    public void generateQuintets() {
        System.out.println("\nSpoustim generaci vsech moznych petic nabidek..." +
                " Muze to chvili trvat(zalezi na velikosti dat). Prosim cekejte.");
        allQuintets = new ArrayList<>(events.length);
        PlanEvent[] currentPlanEvents;
        Plan currentPlan;

        for (int i = 0; i < events.length; i++) {
            for (int j = i + 1; j < events.length; j++) {
                for (int k = j + 1; k < events.length; k++) {
                    for (int l = k + 1; l < events.length; l++) {
                        for (int m = l + 1; m < events.length; m++) {
                            currentPlanEvents = new PlanEvent[]{events[i], events[j], events[k],
                                    events[l], events[m]};
                            currentPlan = new Plan(currentPlanEvents);
                            if (!currentPlan.isConflict() && currentPlan.isOK()) {
                                allQuintets.add(currentPlan);
                            }
                        }
                    }
                }
            }
        }
        System.out.println("Hotovo!\n" +
                "Ukladam nalezene varianty do souboru... Kazda varianta rozvrhu je oddelena dvema prazdnimi radkami.");
        try {
            PrintStream sout = System.out;
            File file = new File(".\\cv2_data\\ssc_out.txt");
            System.setOut(new PrintStream(file));
            allQuintets.forEach(Plan::printTutorsInfo);
            System.setOut(sout);
            System.out.println("Uspesne ulozeno. Cesta k souboru s daty je:\n"
                + file.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Pocet spravnych a bezkonfliktnich rozvrhu je: " + allQuintets.size());
    }
}
