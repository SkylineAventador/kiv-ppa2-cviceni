package Cviceni_05;

public class InsertSort implements ISortingAlgorithm {
    private int compares= 0;
    private long start,end, totalTime = 0;

    private boolean greaterThan(int[] data, int j, int v) {
        compares++;
        return data[j]>v;
    }

    private boolean onlyGreaterThan(int[] data, int j, int v) {
        return data[j]>v;
    }

    public void sort(int[] data) {
        compares = 0;
        for (int i = 1;i<data.length;i++) {
            int v = data[i];
            int j = i-1;
            while((j>=0)&&(greaterThan(data, j, v))) {
                data[j+1] = data[j];
                j--;
            }
            data[j+1] = v;
        }
    }

    public void onlySort(int[] data) {
        start = System.nanoTime();
        for (int i = 1;i<data.length;i++) {
            int v = data[i];
            int j = i-1;
            while((j>=0)&&(onlyGreaterThan(data, j, v))) {
                data[j+1] = data[j];
                j--;
            }
            data[j+1] = v;
        }
        end = System.nanoTime();
        totalTime += (end - start);
    }

    public int comparesInLastSort() {
        return compares;
    }

    public void printTotalTime() {
        System.out.println("\"Insert sort\" sorting time was: " + totalTime + " nanoseconds." +
                " (" + totalTime / 1000000 + " milliseconds)");
        totalTime = 0;
    }

}
