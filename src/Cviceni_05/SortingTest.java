package Cviceni_05;

import java.util.Arrays;
import java.util.Random;

public class SortingTest {
    public static void main(String[] args) {
        ISortingAlgorithm[] algorithms = {new InsertSort(), new QuickSort(), new MergeSort(), new JavaSort()};

        for (ISortingAlgorithm algorithm : algorithms) {
            System.out.println("============TESTING " + algorithm.getClass().getSimpleName() + "============");
            if (testCorrectness(algorithm)) {
                System.out.println("Random values array. Processing, please wait...");
                testCounts(algorithm, false);
                algorithm.printTotalTime();

                System.out.println();

                System.out.println("Already sorted array. Processing, please wait...");
                testCounts(algorithm, true);
                algorithm.printTotalTime();
            }
            System.out.println("========================================");
        }
    }

    private static void testCounts(ISortingAlgorithm algorithm, boolean sortedTest) {
        int MIN_LENGTH = 100;
        int MAX_LENGTH = 100000;
        int TEST_COUNT = 100;
        for (int length = MIN_LENGTH; length < MAX_LENGTH; length *= 2) {
            int minComp = Integer.MAX_VALUE;
            int maxComp = 0;
            for (int test = 0; test < TEST_COUNT; test++) {
                int[] data = generateData(length);
                if (sortedTest)
                    Arrays.sort(data);
                try {
                    algorithm.onlySort(data);
                } catch (StackOverflowError error) {
                    System.err.println("Error: Compares count is too high: " + algorithm.comparesInLastSort());
                    break;
                }
                if (algorithm.comparesInLastSort() > maxComp)
                    maxComp = algorithm.comparesInLastSort();
                if (algorithm.comparesInLastSort() < minComp)
                    minComp = algorithm.comparesInLastSort();
            }
            // TODO: 26.03.2019 Uncomment to print compares values
//            System.out.println("Length: " + length + ", Min:" + minComp + ", Max:" + maxComp);
        }
    }

    private static boolean testCorrectness(ISortingAlgorithm algorithm) {
        for (int i = 0;i<100;i++) {
            int[] data = generateData(100);
//            int[] data = new int[]{5, 2, 3, 4, 1}; //Male testovaci pole.
            int[] dataCopy = data.clone();
            algorithm.sort(data);
            Arrays.sort(dataCopy);
            for(int j = 0;j<data.length;j++) {
                if (data[j]!=dataCopy[j]) {
                    System.out.println("Algorithm failed, terminating.");
                    System.out.println("Algorithm: " + Arrays.toString(data));
                    System.out.println("Java sort: " + Arrays.toString(dataCopy));
                    return false;
                }
            }
        }
        System.out.println("Algorithm passed test, continuing.");
        return true;
    }

    private static int[] generateData(int c) {
        int[] result = new int[c];
        Random rnd = new Random();
        for (int i = 0;i<c;i++)
            result[i] = rnd.nextInt(c);
        return result;
    }
}
