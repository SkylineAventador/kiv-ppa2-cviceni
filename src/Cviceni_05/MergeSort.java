package Cviceni_05;

public class MergeSort implements ISortingAlgorithm {
    private int compares = 0;
    private long start, end, totalTime = 0;

    @Override
    public void sort(int[] data) {
        compares = 0;
        mergeSort(data, 0, data.length);
    }

    @Override
    public void onlySort(int[] data) {
        start = System.nanoTime();
        onlyMergeSort(data, 0, data.length);
        end = System.nanoTime();
        totalTime += (end - start);
    }

    private void mergeSort(int[] data, int start, int length) {
        if (length == 1)
            return;
        int lengthA = length/2;
        mergeSort(data, start, lengthA);
        mergeSort(data, start + lengthA, length - lengthA);
        int[] temp = bitonic(data, start, lengthA, length-lengthA);
        mergeBitonic(data, start, temp);
    }

    private void onlyMergeSort(int[] data, int start, int length) {
        if (length == 1)
            return;
        int lengthA = length/2;
        mergeSort(data, start, lengthA);
        mergeSort(data, start + lengthA, length - lengthA);
        int[] temp = bitonic(data, start, lengthA, length-lengthA);
        mergeBitonic(data, start, temp);
    }

    private int[] bitonic(int[] data, int startA, int lengthA, int lengthB){
        int[] result = new int[lengthA+lengthB];
        compares++;
        for (int i = 0;i<lengthA;i++)
            result[i] = data[startA+i];
        int end = startA+lengthA+lengthB-1;
        compares++;
        for (int i = 0;i<lengthB;i++){
            result[i+lengthA] = data[end-i];
            compares++;
        }
        return result;
    }

    private int[] onlyBitonic(int[] data, int startA, int lengthA, int lengthB){
        int[] result = new int[lengthA+lengthB];
        for (int i = 0;i<lengthA;i++)
            result[i] = data[startA+i];
        int end = startA+lengthA+lengthB-1;
        for (int i = 0;i<lengthB;i++){
            result[i+lengthA] = data[end-i];
        }
        return result;
    }

    private void mergeBitonic(int[] data, int start, int[] bitonic){
        int i = 0;
        int j = bitonic.length-1;
        compares++;
        for (int k = 0;k<bitonic.length;k++) {
            data[start + k] = bitonic[i] < bitonic[j] ?
                    bitonic[i++] : bitonic[j--];
            compares++;
        }
    }

    private void onlyMergeBitonic(int[] data, int start, int[] bitonic){
        int i = 0;
        int j = bitonic.length-1;
        compares++;
        for (int k = 0;k<bitonic.length;k++) {
            data[start + k] = bitonic[i] < bitonic[j] ?
                    bitonic[i++] : bitonic[j--];
            compares++;
        }
    }

    @Override
    public int comparesInLastSort() {
        return compares;
    }

    public void printTotalTime() {
        System.out.println("\"Merge sort\" sorting time was: " + totalTime + " nanoseconds." +
                " (" + totalTime / 1000000 + " milliseconds)");
        totalTime = 0;
    }


}
