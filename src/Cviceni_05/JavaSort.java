package Cviceni_05;

import java.util.Arrays;

public class JavaSort implements ISortingAlgorithm {
    private long start,end, totalTime = 0;

    @Override
    public void sort(int[] data) {
        start = System.nanoTime();
        Arrays.sort(data);
        end = System.nanoTime();
        totalTime += (end - start);
    }

    @Override
    public void onlySort(int[] data) {
        start = System.nanoTime();
        Arrays.sort(data);
        end = System.nanoTime();
        totalTime += (end - start);
    }

    @Override
    public int comparesInLastSort() {
        return 0;
    }

    @Override
    public void printTotalTime() {
        System.out.println("\"Java sort\" sorting time was: " + totalTime + " nanoseconds." +
                " (" + totalTime / 1000000 + " milliseconds)");
        totalTime = 0;
    }
}
