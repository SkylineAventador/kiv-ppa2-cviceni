package Cviceni_05;

public interface ISortingAlgorithm {
    void sort(int[] data);
    void onlySort(int[] data);
    int comparesInLastSort();
    void printTotalTime();
}
