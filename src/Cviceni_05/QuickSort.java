package Cviceni_05;

public class QuickSort implements ISortingAlgorithm {
    private int compares = 0;
    private long start, end, totalTime = 0;

    @Override
    public void sort(int[] data) {
        compares = 0;
        quickSort(data, 0, data.length - 1);
    }

    @Override
    public void onlySort(int[] data) {
        start = System.nanoTime();
        quickSort(data, 0, data.length - 1);
        end = System.nanoTime();
        totalTime += (end - start);
    }

    private void quickSort(int[] data, int start, int end) {
        if (end<=start) return;
        int i = split(data, start, end);
        quickSort(data, start, i-1);
        quickSort(data, i+1, end);
    }

    private void onlyQuickSort(int[] data, int start, int end) {
        if (end<=start) return;
        int i = onlySplit(data, start, end);
        onlyQuickSort(data, start, i-1);
        onlyQuickSort(data, i+1, end);
    }

    private int split(int[] data, int left, int right) {
        int pivot = data[right];
        while (true){
            while ((data[left]<pivot)&&(left<right)) {
                left++;
                compares++;
            }
            compares++;

            if (left<right){
                data[right] = data[left];
                right--;
            } else break;

            while ((data[right]>pivot)&&(left<right)) {
                right--;
                compares++;
            }
            compares++;

            if (left<right){
                data[left] = data[right];
                left++;
            } else break;
        }
        data[left] = pivot;
        return(left);
    }

    private int onlySplit(int[] data, int left, int right) {
        int pivot = data[right];
        while (true){
            while ((data[left]<pivot)&&(left<right)) {
                left++;
            }

            if (left<right){
                data[right] = data[left];
                right--;
            } else break;

            while ((data[right]>pivot)&&(left<right)) {
                right--;
            }

            if (left<right){
                data[left] = data[right];
                left++;
            } else break;
        }
        data[left] = pivot;
        return(left);
    }

    @Override
    public int comparesInLastSort() {
        return compares;
    }

    public void printTotalTime() {
        System.out.println("\"Quick sort\" sorting time was: " + totalTime + " nanoseconds. " +
                "(" + totalTime / 1000000 + " milliseconds)");
        totalTime = 0;
    }


}
