Verze kódu platí pro akademický rok 2018/2019.
Dopuručuji využit tento kód jako vzor a napsat si vlastní na zakladě jeho funkcionality. 
V opačném případě může být vaše řešení zamítnuto validátorem a vy dostanete tak zvaný WARNING, při opakovaném dostání kterého bude vám již ZNEUMOŽNĚNO získání zápočtu z tohoto předmětu.
Pozor: Validátor ověřuje nejen podobnost slov a tak dále, ale celkovou strukturu kódu. Je to dáno tím, že každý člověk si napíše kód jinák. Takže pozměnit jen názvy proměnných a/nebo metod rozhodně nestačí. 
Hodně úspěchů.
