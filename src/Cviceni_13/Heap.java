package Cviceni_13;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class Heap {
    Interval[] values;
    double[] priorities;
    int count = 0;

    public static Comparator<Interval> intervalComparator = new Comparator<Interval>() {
        @Override
        public int compare(Interval o1, Interval o2) {
            return (int) ((o1.end - o1.start) - (o2.end - o2.start));
        }
    };

    public Heap(int capacity){
        values = new Interval[capacity+1];
        priorities = new double[capacity+1];
    }

    void add(Interval e, double priority){
        count++;
        if (count == priorities.length)
            expandArrays();
        priorities[count] = priority;
        values[count] = e;
        fixUp(count);
    }

    void swap(int x, int y) {
        double double_tmp_priorities = priorities[x];
        priorities[x] = priorities[y];
        priorities[y] = double_tmp_priorities;

        Interval interval_tmp_values = values[x];
        values[x] = values[y];
        values[y] = interval_tmp_values;
    }

    void fixUp(int index){
        int n = index;
        while (n != 1) {
            int p = n/2; // predchudce
            if (priorities[p]<priorities[n]) {
                swap(p,n);
                n = p;
            }
            else return;
        }

    }

    private void expandArrays() {
        Interval[] newValues = Arrays.copyOf(values, values.length * 2);
        double[] newPriorities = Arrays.copyOf(priorities, priorities.length * 2);

        values = newValues;
        priorities = newPriorities;

        System.out.println("Expanding has been successfully done.");
    }

    public Interval removeMax() {
        Interval l = values[1];

        values[1] = values[count];
        priorities[1] = priorities[count];

        count--;
        fixDown(1);
        return l;
    }

    private void fixDown(int n) {
        if (2*n<=count) {
            int j = 2*n; // index leveho potomka
            if ((j+1)<=count)
                if (priorities[j+1]>priorities[j])
                    j = j+1; // j je ted index vetsiho potomka
            if (priorities[n]>priorities[j])
                return; // Vlastnost 2 je ok
            else {
                swap(j,n);
                fixDown(j);
            }
        }
    }
}
