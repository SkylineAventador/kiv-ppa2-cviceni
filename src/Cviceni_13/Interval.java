package Cviceni_13;

public class Interval implements Comparable<Interval> {
    double start; // zacatek intervalu
    double end;  // konec intervalu

    public Interval(double start, double end) {
        this.start = start;
        this.end = end;
    }

    public void testPringInfo() {
        System.out.printf("Priority: %.3f\n", (end - start));
    }

    @Override
    public int compareTo(Interval o) {
        double thisLenth = end - start;
        double anotherLength = o.end - o.start;
        if (thisLenth == anotherLength)
            return 0;
        else
            return thisLenth > anotherLength ? 1 : -1;
    }
}
