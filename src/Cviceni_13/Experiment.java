package Cviceni_13;

import java.util.Random;

public class Experiment {
    private static final int HEAP_SIZE = 1000;

    public static void main(String[] args) {
        Random random = new Random();

        Heap test = new Heap(HEAP_SIZE);

        double start, end, priority;
        for (int i = 0; i < HEAP_SIZE; i++) {
            start = random.nextDouble();
            end = random.nextDouble() + start;
            priority = end - start;
            test.add(new Interval(start, end), priority);
        }

        for (int i = 0; i < HEAP_SIZE; i++) {
            test.removeMax().testPringInfo();
        }
    }
}
