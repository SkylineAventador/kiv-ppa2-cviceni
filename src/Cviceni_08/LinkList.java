package Cviceni_08;

public class LinkList {
    Link first, current;
    MyIterator iterator;

	void insert(char letter) {
		Link newLink = new Link();
		newLink.data = letter;
		if (current == null) {
			newLink.next = first;
			first = newLink;
			iterator = new MyIterator(this);
		}
		else {
            newLink.next = current.next;
            current.next = newLink;
		}
	}

	void next() {
		if (first == null)
            System.out.println("Prvni prvek neni inicializovan");;
		if (current == null) {
			current = first;
		}
		else
		{
			current = current.next;
			if (current == null)
                System.out.println("Aktualni prvek neni inicializovan");;
		}
	}

	char get() {
		if (first == null) {
			return ' ';
		}
		if (current == null) {
			return first.data;
		}
		if (current.next!=null) {
			return current.next.data;
		} else return ' ';
	}

	void moveToFirst() {
		current = null;
	}

	boolean hasNext() {
		if (current == null) {
			if (first != null)
				return true;
			else return false;
		}
		if (current.next!=null)
			return true;
		else return false;
	}

    void remove() {
        if (current == null) {
            first = first.next;
        }
        else if (current.next == null) {
            System.out.println("Chyba odstraneni. Nasledujici prvek neni inicializovan");;
        }
        else{
            current.next = current.next.next;
        }
    }
}
