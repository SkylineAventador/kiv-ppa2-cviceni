package Cviceni_08;

import java.io.*;

public class Manipulator {
    private LinkList list;
    private MyIterator iterator;

    private BufferedReader inputReader;
    private BufferedReader instructionsReader;

    private final String OUTPUT_FILE_PATH;

    /**
     * @param input input file path
     * @param intructions instructions file path
     * @param output output file path
     */
    public Manipulator(String input, String intructions, String output) {
        OUTPUT_FILE_PATH = output;
        try {
            inputReader = new BufferedReader(new FileReader(input));
            instructionsReader = new BufferedReader(new FileReader(intructions));
            System.out.println("All files were successfully connected");
        } catch (FileNotFoundException e) {
            System.err.println("Input or instructions file was not found!");
            System.exit(1);
        }
        readInput();
        applyInstructions();
    }

    private void readInput() {
        System.out.println("Reading input file... Please wait.");
        list = new LinkList();
        try {
            String readLine;
            char[] chars;
            do {
                readLine = inputReader.readLine();
                chars = readLine.toCharArray();
                for (char symbol : chars) {
                    list.insert(symbol);
                    list.next();
                }
            } while (!readLine.equals("THE END"));
            inputReader.close();
        } catch (IOException e) {
            System.err.println("Chyba nacteni radky souboru.");
            e.printStackTrace();
        }
        System.out.println("Input file was successfully read.");
    }

    private void applyInstructions() {
        System.out.println("Applying instructions... Please wait.");
        iterator = new MyIterator(list);
        try {
            String actualString = instructionsReader.readLine();
            while (actualString != null) {
                switch (actualString.charAt(0)) {
                    case 'N': {
                        iterator.next();
                        break;
                    }
                    case 'B': {
                        iterator.moveToFirst();
                        break;
                    }
                    case 'I': {
                        iterator.insert(actualString.charAt(2));
                        break;
                    }
                    case 'R': {
                        iterator.remove();
                        break;
                    }
                }
                actualString = instructionsReader.readLine();
            }
            instructionsReader.close();
        } catch (IOException e) {
            System.err.println("Chyba nacteni znaku instrukcniho souboru");
            e.printStackTrace();
        } catch (Exception e){
            System.err.println("Chyba vykonani operaci iteratoru.");
            e.printStackTrace();
        }

        try {
            System.setOut(new PrintStream(new File(OUTPUT_FILE_PATH))); //Switch output to the file.
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        printActualList_toFile(); //Printing actual list (to the file in this case)

        System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out))); //Resetting output back to the console.
        System.out.println("Instructions were successfully applied.\nResult was saved to \"" +
                OUTPUT_FILE_PATH.substring(OUTPUT_FILE_PATH.indexOf('/') + 1) + "\"");
    }

    private void printActualList_toFile() {
        try {
            for (MyIterator it = new MyIterator(list); it.hasNext(); it.next()) {
                System.out.print(it.get());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
