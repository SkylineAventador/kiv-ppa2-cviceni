package Cviceni_08;

public class MyIterator {
    private Link current;
    private LinkList list;

    public MyIterator(LinkList list) {
        this.list = list;
        this.current = null;
    }

    void insert(char letter) {
        Link newLink = new Link();
        newLink.data = letter;
        if (current == null) {
            newLink.next = list.first;
            list.first = newLink;
        }
        else {
            newLink.next = current.next;
            current.next = newLink;
        }
    }

    void next() throws Exception {
        if (list.first == null)
            throw new Exception();
        if (current == null) {
            current = list.first;
        }
        else
        {
            current = current.next;
            if (current == null)
                throw new Exception();
        }
    }

    char get() throws Exception {
        if (list.first == null) {
            throw new Exception();
        }
        if (current == null) {
            return list.first.data;
        }
        if (current.next!=null) {
            return current.next.data;
        }
        else throw new Exception();
    }

    void moveToFirst() {
        current = null;
    }

    boolean hasNext() {
        if (current == null) {
            if (list.first != null)
                return true;
            else return false;
        }
        if (current.next!=null)
            return true;
        else return false;
    }

    void remove() throws Exception {
        if (current == null) {
            list.first = list.first.next;
        } else if (current.next == null) {
            throw new Exception();
        } else {
            current.next = current.next.next;
        }
    }
}
