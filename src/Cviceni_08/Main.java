package Cviceni_08;

public class Main {
    public static void main(String[] args) {
        //Part 3
        //All functionality is processed from this constructor. No further actions are needed.
        //Change files paths to yours if needed.
        Manipulator manipulator = new Manipulator("./cv08_data/input.txt",
                "./cv08_data/instructions.txt", "./cv08_data/output.txt");
    }
}
