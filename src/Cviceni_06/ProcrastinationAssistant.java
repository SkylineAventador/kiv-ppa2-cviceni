package Cviceni_06;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;


public class ProcrastinationAssistant {
    private static final int TESTS_COUNT = 100000;
    public static void main(String[] args){
        IStringStack zasobnik = new StackArray();
        IStringStack pomocnyZasobnik = new StackArray();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String hlavniUkol;

        System.out.println("Co je treba udelat?");
        try {
            zasobnik.add(reader.readLine());
            hlavniUkol = zasobnik.get();
            char answerChar;

            while (!zasobnik.get().isEmpty()) {
                System.out.println("Aktualni ukol: " + zasobnik.get());
                System.out.println("Co s ukolem? (H = Hotovo, R = Rozdelit)");
                answerChar = reader.readLine().charAt(0);
                if (answerChar == 'R') {
                    System.out.println("Zadejte prosim podukoly, ukoncene prazdnym retezcem:");
                    zasobnik.removeLast();
                    while (true) {
                        pomocnyZasobnik.add(reader.readLine());
                        if (pomocnyZasobnik.get().isEmpty()) {
                            pomocnyZasobnik.removeLast();
                            break;
                        }
                        System.out.println("Ukol byl pridan.");
                    }
                    while (!pomocnyZasobnik.get().isEmpty()) {
                        zasobnik.add(pomocnyZasobnik.get());
                        pomocnyZasobnik.removeLast();
                    }
                } else if (answerChar == 'H') {
                    zasobnik.removeLast();
                    System.out.println("Ukol byl splnen.");
                }
            }
            System.out.println("Hlavni ukol " + hlavniUkol + " byl splnen.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String randomString(){
        StringBuilder sb = new StringBuilder();
        Random r = new Random();
        for (int i = 0; i < (5 + r.nextInt(20)); i++) {
            sb.append((char) (r.nextInt(24) + 65));
        }
        return(sb.toString());
    }

    /**
     * Metoda otestuje funguje-li zasobnik spravne.
     * @param stack zasobnik implementujici rozhrani IStringStack
     * @return true = vse je v poradku, false = implementace zasobniku obsahuje chyby.
     */
    private static boolean testStack(IStringStack stack, int testsCount) {
        long start, end;
        System.out.println("Probiha testovani zasobniku. Pocet testovani: " + testsCount + "\n" +
                "Cekejte prosim...");
        String[] testArray = new String[testsCount];
        for (int i = 0; i < testArray.length; i++) {
            testArray[i] = randomString();
        }

        start = System.nanoTime();
        for (int i = 0; i < testsCount; i++) {
            stack.add(testArray[i]);
        }

        for (int i = testsCount - 1; i >= 0; i--) {
            if (!stack.get().equals(testArray[i])) {
                System.out.println("Testovani je ukonceno: Pozor - Implementace zasobniku obsahuje chyby.");
                return false;
            } else
                stack.removeLast();
        }
        end = System.nanoTime();

        System.out.println("Testovani je ukonceno: Zasobnik funguje spravne. (Utraceny cas: " + (end - start) / 1000000
                + " ms nebo " + (end - start) + " nanosekund)");
        return true;
    }
}
