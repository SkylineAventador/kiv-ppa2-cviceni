package Cviceni_06;

public interface IStringStack {
    void add(String s);
    String get();
    void removeLast();
}
