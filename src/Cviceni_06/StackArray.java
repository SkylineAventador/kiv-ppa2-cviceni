package Cviceni_06;

import java.util.Arrays;

public class StackArray implements IStringStack {
    private String[] data;
    private int freeIndex;
    private final boolean IS_SLOW_EXPAND;

    public void add(String s){
        if (freeIndex == data.length) {
            expandArray(IS_SLOW_EXPAND);
        }
        data[freeIndex] = s;
        freeIndex++;
    }

    public StackArray(){
        data = new String[5];
        freeIndex = 0;
        IS_SLOW_EXPAND = false;
    }

    public StackArray(boolean isSlow) {
        data = new String[5];
        freeIndex = 0;
        IS_SLOW_EXPAND = isSlow;
    }

    public String get() {
        if (freeIndex >= 1)
            return data[freeIndex - 1];
        return "";
    }

    public void removeLast() {
        if (freeIndex >= 1)
            freeIndex--;
    }

    private void expandArray(boolean isSlow) {
        String[] tempData;
        if (isSlow) {
            tempData = new String[data.length + 10];
        } else {
            tempData = new String[data.length * 2];
        }
        System.arraycopy(data, 0, tempData, 0, data.length);
        data = tempData;
    }
}
