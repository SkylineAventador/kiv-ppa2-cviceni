package Cviceni_04;

import com.sun.istack.internal.NotNull;

import java.util.Arrays;
import java.util.Random;

public class RemoveDuplicites {

    static long[] starts = new long[3];
    static long[] ends = new long[3];
    static long[] slowest = new long[]{0, 0};

    /**
     * Odstrani z pole prvek na indexu i
     * @param data vstupni pole dat
     * @param index index polozky ktera se ma odstranit
     * @return nove pole bez jedne polozky
     */
    public static int[] removeItem(int[] data, int index){
        // vysledne pole bude o 1 kratsi
        int[] result = new int[data.length-1];
        // zkopirujeme prvky az do indexu i
        for (int i = 0;i<index;i++)
            result[i] = data[i];
        // i-ty prvek preskocime a zkopirujeme vsechny zbyvajici prvky
        for (int i = index + 1;i<data.length;i++)
            result[i-1] = data[i];
        return result;
    }

    /**
     * Prochazi vsechny polozky a odstranuje duplikaty metodou removeItem
     * @param data vstupni pole
     * @return data s odstranenymi duplikaty
     */
    static int[] removeDuplicates1(int[] data){
        int[] result = data;
        for (int i = 0;i<result.length;i++){
            for (int j = i+1;j<result.length;j++){
                if (result[j] == result[i]){
                    result = removeItem(result, j);
                    j--;
                }
            }
        }
        return result;
    }

    /**
     * Prochazi vsechny polozky a provadi ostraneni vsech duplikatu jedne polozky najednou
     * @param data vstupni pole
     * @return data s odstranenymi duplikaty
     */
    static int[] removeDuplicates2(int[] data){
        int[] result = data;
        for(int i = 0;i<result.length;i++){
            // spocteme kolik ma polozka result[i] duplikatu
            int count = 0; // pocet duplikatu
            for (int j = i+1;j<result.length;j++) {
                if (result[j] == result[i])
                    count++;
            }
            // pokud je alespon jeden duplikat, pak ho odstranime
            if (count>0){
                // vysledek bude o count kratsi
                int[] newResult = new int[result.length-count];
                // prvky az do indexu i muzeme jednoduse zkopirovat
                for (int k = 0;k <= i;k++)
                    newResult[k] = result[k];
                int index = i+1; // index v cilovem poli
                for (int k = i+1;k<result.length;k++){
                    if (result[k]!=result[i]){
                        // neni duplikat
                        newResult[index] = result[k];
                        index++;
                    }
                }
                result = newResult;
            }
        }
        return result;
    }

    /**
     * Pouziva redukci pomoci pole zaznamu, zda dane cislo bylo nalezeno v datech ci nikoli
     * @param data vstupni pole
     * @return data s odstranenymi duplikaty
     */
    static int[] removeDuplicates3(int[] data){
        // nejdrive jen zjistime, kolik mame unikatnich cisel
        boolean[] encountered = new boolean[1000000];
        int count = 0; // pocet unikatnich cisel
        for (int i = 0;i<data.length;i++){
            if (!encountered[data[i]]){
                // nove objevene cislo
                encountered[data[i]] = true;
                count++;
            }
        }
        // v promenne count je ted pocet unikatnich cisel
        // pole encountered ted pouzijeme jeste jednou stejnym zpusobem
        encountered = new boolean[1000000];
        int[] result = new int[count];
        int index = 0;
        for (int i = 0;i<data.length;i++){
            if (!encountered[data[i]]){
                result[index] = data[i];
                encountered[data[i]] = true;
                index++;
            }
        }
        return result;
    }

    /**
     * generuje nahodna data v rozsahu do 100 000,
     * cimz se simuluje, ze cca 90% cisel je "neaktivnich"
     * @param count pocet pozadovanych cisel
     * @return pole nahodnych cisel
     */
    static int[] generateData(int count)
    {
        int[] result = new int[count];
        Random r = new Random();
        for (int i = 0;i<result.length;i++)
            result[i] = r.nextInt(100000);
        return result;
    }

    public static void main(String[] args)
    {
        int count = 1000;
        int[] data;
        try {
            for (int i = 0; i < 3; i++) {
                do {
                    data = generateData(count);

                    starts[i] = System.currentTimeMillis();
                    int[] reducedData = callMehtod(data, i);
                    ends[i] = System.currentTimeMillis();

                    count *= 2;
                } while (computeTime(i + 1, data.length) < 1000);
                count = 1000;

                if (slowest[1] < ends[i] - starts[i]) {
                    slowest[0] = i;
                    slowest[1] = ends[i] - starts[i];
                }
            }
        } catch (OutOfMemoryError error) {
            System.out.println("Chyba: Dosla pamet.\n" +
                    "Metoda #3 zpracovava data stejne rychle i pri jejich rapidnim zvetseni.");
        }

        System.out.println("Nejpomalejsi je: removeDuplicates" + (slowest[0] + 1) + ".");
        count = 1000;
        System.out.println("Probiha vypocet, cekejte prosim...");
        do {
            data = generateData(count);

            starts[(int)slowest[0]] = System.currentTimeMillis();
            int[] reducedData = callMehtod(data, (int) slowest[0]);
            ends[(int) slowest[0]] = System.currentTimeMillis();

            count *= 2;
        } while (computeSlowest((int)slowest[0], data.length) < 10000);

        printTable();
        System.out.println("All done.");
    }

    private static int[] callMehtod(int[] data, int index) {
        switch (index) {
            case 0:
                return removeDuplicates1(data);
            case 1:
                return removeDuplicates2(data);
            case 2:
                return removeDuplicates3(data);
        }
        return null;
    }

    private static long computeTime(int methodIndex, int length) {
        long result;
        switch (methodIndex) {
            case 1:{
                result = (ends[0] - starts[0]);
                if (result > 1000) {
                    System.out.println("Metoda removeDuplicates" + methodIndex
                            + " trvala pres 1 sekundu (konkretne "
                            + result + " ms) pro n = " + length + ".");
                }
                return result;
            }

            case 2:{
                result = (ends[1] - starts[1]);
                if (result > 1000) {
                    System.out.println("Metoda removeDuplicates" + methodIndex
                            + " trvala pres 1 sekundu (konkretne "
                            + result + " ms) pro n = " + length + ".");
                }
                return result;
            }

            case 3:{
                result = (ends[2] - starts[2]);
                if (result > 1000) {
                    System.out.println("Metoda removeDuplicates" + methodIndex
                            + " trvala pres 1 sekundu (konkretne "
                            + result + " ms) pro n = " + length + ".");
                }
                return result;
            }
        }
        return 0;
    }

    private static long computeSlowest(int methodIndex, int length) {
        long result = (ends[methodIndex] - starts[methodIndex]);
        if (result > 10000) {
            System.out.println("Metoda removeDuplicates" + (methodIndex + 1)
                    + " trvala pres 10 sekund (konkretne "
                    + result + " ms) pro n = " + length + ".");
        }
        return result;
    }

    private static void printTable() {
        int n = 1000;
        int[] data, temp;
        double t1, a1, t2, a2, t3, a3, slowest;
        System.out.println("n\t\tt1\t\ta1\t\tt2\t\ta2\t\tt3\t\ta3");

        do {
            data = generateData(n);

            starts[0] = System.currentTimeMillis();
            temp = removeDuplicates1(data);
            ends[0] = System.currentTimeMillis();
            t1 = (double)(ends[0] - starts[0])/1000;


            starts[1] = System.currentTimeMillis();
            temp = removeDuplicates2(data);
            ends[1] = System.currentTimeMillis();
            t2 = (double)(ends[1] - starts[1])/1000;


            starts[2] = System.currentTimeMillis();
            temp = removeDuplicates3(data);
            ends[2] = System.currentTimeMillis();
            t3 = (double)(ends[2] - starts[2])/1000;


            slowest = Math.max(Math.max(t1, t2), t3);

            a1 = slowest / t1;
            a2 = slowest / t2;
            a3 = slowest / t3;
            System.out.printf("%d\t%.3fs\t%.3fx\t%.3fs\t%.3fx\t%.3fs\t%.3fx\n", n, t1, a1, t2, a2, t3, a3);

            n += 11000;
        } while (n <= 100000);
    }

}
