package Cviceni_03;

public interface IFunction {
    double valueAt(double p);

    /**
     * Vraci hodnotu derivace v bode x.
     * @param x bod
     * @return hodnota derivace v bode.
     */
    double differentiate(double x);
}
