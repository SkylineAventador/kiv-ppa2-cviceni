package Cviceni_03;

public class QuadraticPolynomial extends AbstractFunction {
    private int a,b,c;

    public QuadraticPolynomial(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double valueAt(double p) {
        return (a * (p * p) + b * p + c);
    }
}
