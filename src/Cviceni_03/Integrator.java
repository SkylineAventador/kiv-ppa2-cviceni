package Cviceni_03;

public class Integrator {
    private double delta;

    double integrate(IFunction f, double a, double b) {
        double result = 0;
        double p = a;
        double v = f.valueAt(p);
        while(p+delta<b) {
            // obdelniky sirky delta
            result += delta * v;
            p += delta;
            v = f.valueAt(p);
        }
        // jeste posledni obdelnik, ktery bude uzsi nez delta
        result += (b-p) * v;
        return result;
    }

    void setDelta(double d)
    {
        this.delta = d;
    }
}
