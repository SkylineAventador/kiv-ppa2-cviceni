package Cviceni_03;

public class Exercise03 {
    public static void main(String[] args) {
        QuadraticPolynomial qf = new QuadraticPolynomial(1, 0, 0);
        qf.setEpsilon(0.01);
        System.out.println(qf.differentiate(2));
    }
}