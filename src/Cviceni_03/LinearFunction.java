package Cviceni_03;

public class LinearFunction extends AbstractFunction{
        double k, q;

        public double valueAt(double p) {
            return(k*p+q);
        }

        public LinearFunction(double k, double q) {
            this.k = k;
            this.q = q;
        }
}
