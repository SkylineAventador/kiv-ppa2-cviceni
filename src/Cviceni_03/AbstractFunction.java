package Cviceni_03;

public abstract class AbstractFunction implements IFunction {
    private double epsilon;
    double result = 0, h = 0.01, result2;
    public double differentiate(double x){

        result = (valueAt(x + h) - valueAt(x)) / h;
        h /= 2;
        result2 = ((valueAt(x + h) - valueAt(x)) / h);

        while (Math.abs(result-result2) >= epsilon) {
            result=result2;
            h /= 2;
            result2 = (valueAt(x + h) - valueAt(x)) / h;
        }

        return result;
    }

    public void setEpsilon(double e) {
        epsilon = e;
    }
}
