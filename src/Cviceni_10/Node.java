package Cviceni_10;

public class Node {
    public String key; // jmeno souboru
    public Node left, right; // potomci
    public Node(String key){
        this.key = key;
    }
}
