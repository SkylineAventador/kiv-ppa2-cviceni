package Cviceni_10;

import java.io.*;
import java.util.ArrayList;

public class BinarySearchTree {
    //2i, 2i+1 - formule nalezeni prvku v reprezentaci polem
    Node root;

    // prida do BST uzel s daným klíčem
    void add(String key){
        if (root == null)
            root = new Node(key);
        else
            addUnder(root, key);
    }

    // vlozi pod uzel n novy uzel s klicem key
    void addUnder(Node n, String key) {
        if (key.compareToIgnoreCase(n.key) < 0) {
            // uzel patri doleva, je tam misto?
            if (n.left != null)
                addUnder(n.left, key);
            else
                n.left = new Node(key);
        } else {
            // uzel patri doprava, je tam misto?
            if (n.right != null)
                addUnder(n.right, key);
            else
                n.right = new Node(key);
        }
    }

    boolean contains(String key) {
        return get(key) != null;
    }

    Node get(String key) {
        Node n = root;
        while (n != null) {
            if (n.key.compareToIgnoreCase(key) == 0) {
                return n;
            }
            if (n.key.compareToIgnoreCase(key) > 0) {
                n = n.left;
            } else
                n = n.right;
        }
        return null;
    }

    void printSorted() {
        getSortedKeys().forEach(System.out::println);
    }

    void printAllStartingWith(String prefix) {
        Node n = root;
        while (n != null) {
            if (n.key.contains(prefix)) {
                printAllStartingWith_Service(n);
                break;
            }
            if (n.key.compareToIgnoreCase(prefix) > 0) {
                n = n.left;
            } else
                n = n.right;
        }
    }

    private void printAllStartingWith_Service(Node currentRoot) {
        if (currentRoot != null) {
            System.out.println(currentRoot.key);
            printAllStartingWith_Service(currentRoot.left);
            printAllStartingWith_Service(currentRoot.right);
        }
    }

    ArrayList<String> getSortedKeys(){
        ArrayList<String> result = new ArrayList<String>();

        getSortedValuesR(root, result);
        return result;
    }

    private void getSortedValuesR(Node n, ArrayList<String> result){
        if(n!=null) {
            getSortedValuesR(n.left, result);
            result.add(n.key);
            getSortedValuesR(n.right, result);
        }
    }

    public void remove(String key) {
        Node n = root; // uzel ktery chceme odebrat
        Node pred = null; // predek uzlu ktery chceme odebrat
        while(!key.equals(n.key)) {
            pred = n;
            if (key.compareTo(n.key)<0)
                n = n.left;
            else n = n.right;
        }
        if ((n.left == null)||(n.right==null)) {
            Node replacement = n.left;
            if (n.right != null)
                replacement = n.right;
            if (pred == null)
                root = replacement;
            else if (pred.left == n)
                pred.left = replacement;
            else pred.right = replacement;
        } else {
            Node leftMax = n.left;
            Node leftMaxPred = n;
            while (leftMax.right!=null) {
                leftMaxPred = leftMax;
                leftMax = leftMax.right;
            }
            n.key = leftMax.key;

            if (leftMax != n.left)
                leftMaxPred.right = leftMax.left;
            else
                n.left = leftMax.left;
        }

    }

    private void resetTree() {
        this.root = null;
    }

    public void readDataFromFile(String filePath) {
        resetTree();

        BufferedReader reader;
        String readString;
        try {
            reader = new BufferedReader(new FileReader(filePath));
            System.out.println("Vstupni soubor byl uspesne pripojen. Nacitam data. Cekejte prosim.");
            readString = reader.readLine();
            while (readString != null) {
                switch (readString.charAt(0)) {
                    case 'A':{
                        add(readString.substring(2));
                        break;
                    }
                    case 'R':{
                        remove(readString.substring(2));
                        break;
                    }
                    case 'P':{
                        printAllStartingWith(readString.substring(2));
                        break;
                    }
                }
                readString = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
