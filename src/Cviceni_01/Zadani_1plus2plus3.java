package Cviceni_01;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Random;
import java.util.Scanner;


public class Zadani_1plus2plus3 {
    /**
     * Metoda rozhoduje, zda se predana hodnota x nachazi v predanem serazenem poli
     * @param data vstupni pole serazenych hodnot od nejmensi po nejvetsi
     * @param x    hledana hodnota
     * @return true, kdyz se x nachazi v poli data, jinak false
     */
    static boolean intervalSubdivision(int[] data, int x){
        int left = 0; //leva hranice intervalu
        int right = data.length - 1; // prava hranice intervalu
        int mid = (left+right)/2; // index uprostred intervalu
        while(data[mid]!=x){
            if (left >= right){
                return(false);
            }
            // nyni zmensime interval
            if (data[mid]>x){
                right = mid-1;
            }
            else{
                left = mid+1;
            }
            mid = (left+right)/2;
        }
        return(true); //
    }

    public static int[] generateNumbers() {
        Random random = new Random();
        int pocet = 10000 + random.nextInt(90000);
        //int pocet = 10 + random.nextInt(90);

        int[] pole = new int[pocet];
        int last = 0;
        for (int i = 0; i < pocet; i++) {
            pole[i] = last + random.nextInt(pocet )+ 1;
            last = pole[i];
        }

        return pole;
    }

    public static boolean sequentialSearch(int[] data, int x) {
        for (int i = 0; i < data.length; i++) {
            if (data[i] == x)
                return true;
        }
        return false;
    }

    /**
     * Metoda nacita celociselne hodnoty ze zadaneho textoveho souboru.
     * Kazda hodnota musi byt na zvlastni radce.
     * @param sc scanner pro nacitani hodnoty. Je predan z volajici metody.
     * @param fileName jmeno textoveho souboru, napriklad seq1.txt
     * @return pole celociselnych hodnot nactenych ze predaneho souboru.
     */
    private static int[] readFile(Scanner sc, String fileName) {
        int[] returnArray = new int[0];
        try {
            sc = new Scanner(new File(fileName));
            int counter = 0;
            while (sc.hasNextLine()) {
                if(sc.nextLine().equals(""))
                    break;
                else
                    counter++;
            }
            sc.close();
            sc = null;

            sc = new Scanner(new File(fileName));
            returnArray = new int[counter];
            for (int i = 0; i < returnArray.length; i++) {
                returnArray[i] = Integer.parseInt(sc.next());
            }

            return returnArray;
        } catch (FileNotFoundException ex) {
            System.out.println("Necessary file wasn't found. Please, check path to it.");
        }
        return returnArray;
    }

    /**
     * Metoda overuje je-li poskytovana posloupnost skutecne serazena.
     * @param data posloupnost celych cisel nahodne delky od 10 000 az 100 000 v rozsahu
     *             hodnot od 0 az delka poslopnosti.
     * @return true je-li posloupnost serazena, jinak false.
     */
    private static boolean isSorted(int[] data) {
        for (int i = 0; i < data.length - 1; i++) {
            if (data[i] > data[i + 1]) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner sc = null;
        int[][] arrays = new int[10][];

        System.out.println("Uploading files - please wait...");
        for (int i = 0; i < arrays.length; i++) {
            arrays[i] = readFile(sc, "./cv1_data/seq" + (i + 1) + ".txt");
        }
        System.out.println("Files upload - Success!");

        int counter = 1, sequenceMaxValue = 0;
        long start, end, isResult, ssResult;
        Random random = new Random();

        for (int[] sequence: arrays) {
            if (isSorted(sequence)) {
                System.out.println("Sequence from file seq" + counter + ".txt is sorted.\n" +
                        "Values capacity is: " + sequence.length);

                sequenceMaxValue = arrays[counter - 1][sequence.length - 1];

                start = System.currentTimeMillis();
                for (int i = 0; i < 10000; i++) {
                    intervalSubdivision(sequence, random.nextInt(sequenceMaxValue));
                }
                end = System.currentTimeMillis();

                isResult = end - start;
                System.out.println("Interval subdivision result for seq" + counter + ".txt is "
                        + isResult + "ms.");

                start = System.currentTimeMillis();

                for (int i = 0; i < 10000; i++) {
                    sequentialSearch(sequence, random.nextInt(sequenceMaxValue));
                }
                end = System.currentTimeMillis();

                ssResult = end - start;

                System.out.println("Sequential search result for seq" + counter + ".txt is "
                        + ssResult + "ms.");
                System.out.println("For this sequence \"Interval Subdivison\" algorithm is faster " +
                        "at ~" + Math.round(ssResult / isResult) + "x times than \"Sequential search\" algorithm.\n");

            } else {
                System.out.println("Sequence from file seq" + counter + ".txt isn't sorted!\n");
            }
            counter++;
        }
    }
}
