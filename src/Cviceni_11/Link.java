package Cviceni_11;

public class Link {
    int neighbour;
    Link next;

    public Link(int n, Link next) {
        this.neighbour = n;
        this.next = next;
    }
}
