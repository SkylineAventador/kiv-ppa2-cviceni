package Cviceni_11;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.*;

public class Graph {
    Link[] edges;
    private int verticesCount;

    public void initialize(int vertexCount){
        this.edges = new Link[vertexCount];
        this.verticesCount = vertexCount;
    }

    public void addEdge(int start, int end) {
        edges[start] = new Link(end, edges[start]);
        edges[end] = new Link(start, edges[end]);
    }

    public int shortestPath(int start, int end) {
        int[] result = new int[edges.length];
        for (int i = 0;i<edges.length;i++)
            result[i] = -1;
        result[start] = 0;

        int[] mark = new int[verticesCount];
        mark[start] = 1;

        Queue<Integer> q = new LinkedList<>();
        q.add(start);

        while(!q.isEmpty()) {
            int v = q.poll();
            ArrayList<Integer> nbs = neighbours(v);
            for (int n : nbs) {
                if (mark[n] == 0) {
                    mark[n] = 1;
                    q.add(n);
                    result[n] = result[v] + 1;
                    if (n == end)
                        return result[end];
                }
            }
            mark[v] = 2;
        }
        return -1;
    }

    private ArrayList<Integer> neighbours(int vertexIndex){
        ArrayList<Integer> result = new ArrayList<>();
        Link n = edges[vertexIndex];
        while(n!=null) {
            result.add(n.neighbour);
            n = n.next;
        }
        return result;
    }

    private boolean isNeighbour(int i, int j){
        Link n = edges[i];
        while(n!=null) {
            if (n.neighbour == j)
                return true;
            n = n.next;
        }
        return false;
    }

    public int farthestVertex(int start) {
        Integer[] result = new Integer[edges.length];
        for (int i = 0;i<edges.length;i++)
            result[i] = -1;
        result[start] = 0;
        int[] mark = new int[verticesCount];
        mark[start] = 1;
        Queue<Integer> q = new LinkedList<>();
        q.add(start);

        while(!q.isEmpty()) {
            int v = q.poll();
            ArrayList<Integer> nbs = neighbours(v);
            for (int n : nbs) {
                if (mark[n] == 0) {
                    mark[n] = 1;
                    q.add(n);
                    result[n] = result[v] + 1;
                }
            }
            mark[v] = 2;
        }
        return Collections.max(Arrays.asList(result));
    }

    public void loadFromFile(String filePath) {
        FileReader reader;
        try {
            reader = new FileReader(filePath);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //Unfortunately =(
    }

}
