package Cviceni_12;

import java.util.ArrayList;

public class Spreadsheet {
    private Cell[] cells;
    private Graph graph;

    public Spreadsheet(Cell[] cells) {
        graph = new Graph();
        graph.init(cells.length);
        this.cells = cells;
    }

    public void addDependence(String cell1, String cell2) {
        int i1 = 0;

        for (int i = 0; i < cells.length; i++) {
            if (cells[i].location.equals(cell1)){
                i1 = i;
                break;
            }
        }

        int i2 = 0;

        for (int i = 0; i < cells.length; i++) {
            if (cells[i].location.equals(cell2)){
                i2 = i;
                break;
            }
        }

        graph.addEdge(i1, i2);
    }

    public boolean hasCycle() {
        return graph.hasCycle();
    }

    public static void main(String[] args) {
        Cell[] cells = new Cell[4];
        cells[0] = new Cell("A1");
        cells[1] = new Cell("A2");
        cells[2] = new Cell("B1");
        cells[3] = new Cell("B2");

        Spreadsheet sheet = new Spreadsheet(cells);

        sheet.addDependence("A1", "A2"); // A1 zavisi na A2
        sheet.addDependence("A2", "B1");
        sheet.addDependence("B1", "B2");
//        sheet.addDependence("B2", "B1"); //Uncomment to get cycle in graph and test hasCycle() method


        System.out.println("Does graph has cycle: " + sheet.hasCycle());
        sheet.evaluationOrder();
    }

    public void evaluationOrder() {
        ArrayList<Integer> arrayList = graph.topologicalOrdering();
        for (int i = 0; i < arrayList.size(); i++) {
            System.out.println(cells[arrayList.get(i)].location);
        }
    }
}
