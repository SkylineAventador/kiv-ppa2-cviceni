package Cviceni_12;

import java.util.ArrayList;

public class Graph {
    Link[] edges;
    private int[] mark;

    public void init(int vertexCount) {
        edges = new Link[vertexCount];
        mark = new int[edges.length];
    }

    public void addEdge(int a, int b) {
        Link nl = new Link();
        nl.neighbour = b;
        nl.next = edges[a];
        edges[a] = nl;
    }

    public boolean hasCycle() {
        return DFS_HasCycleAllComponents();
    }

    private boolean DFS_HasCycleAllComponents() {
        int s = 0;
        mark = new int[edges.length];
        while (s<edges.length){
            if (mark[s] == 0)
                if (DFS_HasCycleOneComponent(s))
                    return true;
            s++;
        }
        return false;
    }

    private boolean DFS_HasCycleOneComponent(int start) {
        mark[start] = 1;
        ArrayList<Integer> nbs = neighbours(start);

        for (int i = 0; i < nbs.size(); i++) {
            int n = nbs.get(i);
            if (mark[n] == 1) return true;
            if (mark[n] == 0)
                if (DFS_HasCycleOneComponent(n))
                    return true;
        }
        mark[start] = 2;
        return(false);
    }

    private ArrayList<Integer> neighbours(int vertexIndex){
        ArrayList<Integer> result = new ArrayList<>();
        Link n = edges[vertexIndex];
        while(n!=null) {
            result.add(n.neighbour);
            n = n.next;
        }
        return result;
    }

    public ArrayList<Integer> topologicalOrdering() {
        int s = 0;
        int[] mark = new int[edges.length];
        ArrayList<Integer> result = new ArrayList<Integer>();
        while (s < edges.length) {
            if (mark[s] == 0) {
                topoOrdPartitial(s, result, mark);
            }
            s++;
        }
        return result;
    }

    private void topoOrdPartitial(int s, ArrayList<Integer> result, int[] mark) {
        mark[s] = 1;
        ArrayList<Integer> nbs = new ArrayList<Integer>();
        Link current = edges[s];
        while (current != null) {
            nbs.add(current.neighbour);
            current = current.next;
        }
        for (int i = 0; i < nbs.size(); i++) {
            int n = nbs.get(i);
            if (mark[n] == 0) {
                topoOrdPartitial(n, result, mark);
            }
        }
        mark[s] = 2;
        result.add(s);
    }
}
