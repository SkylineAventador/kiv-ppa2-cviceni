package Cviceni_07;

import java.io.*;

public class CallerQueue implements IReadable {
    private Link first, last;

    public CallerQueue(String filePath) {
        readFromFile(filePath);
    }

    public CallerQueue() {
    }

    public void add(IncomingCall call) {
        Link nl = new Link();
        nl.data = call;
        if (first == null) {
            first = nl;
            last = nl;
        }
        else {
            last.next = nl;
            last = nl;
        }
    }

    public IncomingCall get() {
        if (first != null)
            return first.data;
        else
            return null;
    }

    public void removeFirst() {
        if (first!=null)
            first = first.next;
        else System.out.println("Remove call on empty queue. Probably error, continuing...");
    }

    @Override
    public void readFromFile(String filePath) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            System.err.println("File " + filePath + " was not found!");
            System.exit(1);
        }
        String readLine = "";
        do {
            try {
                readLine = reader.readLine();
            } catch (IOException e) {
                System.err.println("Chyba nacteni radky");
                e.printStackTrace();
            }
            if (readLine.startsWith("C")) {
                add(new IncomingCall(
                        Integer.parseInt(readLine.substring(readLine.indexOf(" ") + 1, readLine.lastIndexOf(" "))), // Od prvni mezery do dalsi podle formatu radky.
                        Integer.parseInt(readLine.substring(readLine.lastIndexOf(" ") + 1)))); // Od posledni mezery az ke konci.
            }
        } while (!readLine.equals(""));
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
