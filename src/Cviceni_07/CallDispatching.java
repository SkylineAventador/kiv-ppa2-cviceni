package Cviceni_07;

import java.io.*;

public class CallDispatching {
    private static final String INPUT_FILE_PATH = "./cv07_data/callCentrum.txt"; //Change this to your file path.
    private static final String OUTPUT_FILE_PATH = "./cv07_data/dispatching.txt"; //Change this to your file path.

    public static void main(String[] args) {
        //Leave constructors attributes empty to create empty queues.
        CallerQueue callerQueue = new CallerQueue(INPUT_FILE_PATH);
        OperatorQueue operatorQueue = new OperatorQueue(INPUT_FILE_PATH);


        Dispatcher dispatcher = new Dispatcher(callerQueue, operatorQueue); // Pushing inside already filled queues.
        // Queues are being filled inside their constructors.
        try {
            PrintStream printWriter = new PrintStream(new File(OUTPUT_FILE_PATH));
            System.setOut(printWriter); //Redirecting the output stream to file.
            while (dispatcher.dispatchQueue()){ // All the service is being processed inside this method.
                System.out.println(); // Make an empty line between records.
            }
            System.setOut(new PrintStream(new FileOutputStream(FileDescriptor.out))); // Resetting the output stream back to the console.
            System.out.println("Vysledky byly uspesne zapsany do souboru \""
                    + OUTPUT_FILE_PATH.substring(OUTPUT_FILE_PATH.lastIndexOf('/') + 1) + "\"");
            dispatcher.printLongestWaitingInfo();
            System.out.println("All done");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Uncomment this and insert your data type to test Generic Queue implementation.
        /*
        GenericQueue<IncomingCall> queue = new GenericQueue<>();
        int multiplier = 100000;
        for (int i = 0; i < 10; i++) {
            queue.add(new IncomingCall((i + 1), (i + 1) * multiplier));
        }
        for (int i = 0; i < 10; i++) {
            System.out.println("Time: " + queue.get().time + " sec. Number: " + queue.get().callingNumber);
            queue.removeFirst();
        }
        */
    }
}
