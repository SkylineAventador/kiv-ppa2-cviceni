package Cviceni_07;

public class Dispatcher {
    private CallerQueue callerQueue;
    private OperatorQueue operatorQueue;
    private int longestTimeWaited;
    private IncomingCall longestWaitingCall;
    private FreeOperator LWC_Operator;

    public Dispatcher() {
        this.callerQueue = new CallerQueue();
        this.operatorQueue = new OperatorQueue();
    }

    public Dispatcher(CallerQueue callerQueue, OperatorQueue operatorQueue) {
        this.callerQueue = callerQueue;
        this.operatorQueue = operatorQueue;
    }

    public void call(int number, int time) {
        IncomingCall call = new IncomingCall();
        call.callingNumber = number;
        call.time = time;
        callerQueue.add(call);
    }

    public void freeOperator(String name, int time) {
        operatorQueue.add(new FreeOperator(name, time)); // operator name se time sekund od zacatku smeny prihlasil jako dostupny
    }

    /**
     * Method from the exercise.
     */
    public void dispatchCall() {
        if (callerQueue.get() != null && operatorQueue.get() != null) {
            assignCall(callerQueue.get(), operatorQueue.get());
            callerQueue.removeFirst();
            operatorQueue.removeFirst();
        }
    }

    /**
     * Analog method from homework.
     * @return true - if queues aren't empty and they were dispatched; false - if they are empty and weren't dispatched.
     */
    public boolean dispatchQueue() {
        if (callerQueue.get() != null && operatorQueue.get() != null) {
            assignCall(callerQueue.get(), operatorQueue.get());
            callerQueue.removeFirst();
            operatorQueue.removeFirst();
            return true;
        }
        return false;
    }

    private void assignCall(IncomingCall call, FreeOperator operator) {
        String timeWaited = "" + Math.max(0, operator.getTime() - call.time);
        System.out.println(operator.getName() + " is answering call from +420 "+call.callingNumber);
        System.out.println("The caller has waited for " + timeWaited + " seconds.");
        if (Integer.parseInt(timeWaited) >= longestTimeWaited) {
            longestTimeWaited = Integer.parseInt(timeWaited);
            longestWaitingCall = call;
            LWC_Operator = operator;
        }
    }

    public void printLongestWaitingInfo() {
        System.out.println("Nejdele cekal volajici s telefonnim cislem: +420 " + longestWaitingCall.callingNumber + "\n" +
                "Cekal " + longestTimeWaited + " sekund. Byl obslouzen operatorem : " + LWC_Operator.getName());
    }
}
