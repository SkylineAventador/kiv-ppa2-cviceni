package Cviceni_07;

public class FreeOperator {
    private String name;
    private int time;

    public FreeOperator(String name, int time) {
        this.name = name;
        this.time = time;
    }

    public FreeOperator() {
    }

    public String getName() {
        return name;
    }

    public int getTime() {
        return time;
    }
}
