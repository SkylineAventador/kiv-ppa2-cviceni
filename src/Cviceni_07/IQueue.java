package Cviceni_07;

public interface IQueue<T> {
    void add(T e);
    T get();
    void removeFirst();
}
