package Cviceni_07;

public interface IReadable {
    void readFromFile(String filePath);
}
