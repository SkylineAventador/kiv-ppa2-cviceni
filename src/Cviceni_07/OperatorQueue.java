package Cviceni_07;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class OperatorQueue implements IReadable{
    private Link first, last;

    public OperatorQueue(String filePath) {
        readFromFile(filePath);
    }

    public OperatorQueue() {
    }

    public void add(FreeOperator operator) {
        Link nl = new Link();
        nl.operatorData = operator;
        if (first == null) {
            first = nl;
            last = nl;
        }
        else {
            last.next = nl;
            last = nl;
        }
    }

    public FreeOperator get() {
        if (first != null)
            return first.operatorData;
        else
            return null;
    }

    public void removeFirst() {
        if (first!=null)
            first = first.next;
        else System.out.println("Remove operator on empty queue. Probably error, continuing...");
    }

    @Override
    public void readFromFile(String filePath) {
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(filePath));
        } catch (FileNotFoundException e) {
            System.err.println("File " + filePath + " was not found!");
            System.exit(1);
        }
        String readLine = "";
        do {
            try {
                readLine = reader.readLine();
            } catch (IOException e) {
                System.err.println("Chyba nacteni radky");
                e.printStackTrace();
            }
            if (readLine.startsWith("O")) {
                add(new FreeOperator(
                        readLine.substring(readLine.lastIndexOf(" ") + 1), // Od posledni mezery az ke konci.
                        Integer.parseInt(readLine.substring(readLine.indexOf(" ") + 1, readLine.lastIndexOf(" ")))// Od prvni mezery do dalsi podle formatu radky.
                ));
            }
        } while (!readLine.equals(""));
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
