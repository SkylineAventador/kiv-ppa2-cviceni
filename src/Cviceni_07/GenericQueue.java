package Cviceni_07;

public class GenericQueue<T> implements IQueue<T> {
    private GenericLink<T> first, last;

    public void add(T t) {
        GenericLink<T> nl = new GenericLink<>();
        nl.data = t;
        if (first == null) {
            first = nl;
            last = nl;
        }
        else {
            last.next = nl;
            last = nl;
        }
    }


    public T get() {
        if (first != null)
            return first.data;
        else
            return null;
    }


    public void removeFirst() {
        if (first!=null)
            first = first.next;
        else
            System.out.println("Remove on empty queue. Probably error, continuing...");
    }
}
